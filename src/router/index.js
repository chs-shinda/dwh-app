import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/views/dashboard/Index'
import Query from '@/views/query/Index'
import Backup from '@/views/backups/Index'
import Summary from '@/views/summary/Index'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', redirect: '/dashboard' },
    { path: '/summary', name: 'summary', component: Summary },
    { path: '/dashboard', name: 'dashboard', component: Dashboard },
    { path: '/query', name: 'query', component: Query },
    { path: '/backups', name: 'backups', component: Backup },
    { path: '*', redirect: '/dashboard' }
  ]
})
