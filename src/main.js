import Vue from 'vue'
import Vuetify from 'vuetify'
import VueHighcharts from 'vue-highcharts'
import 'vuetify/dist/vuetify.css'

import App from './App'
import router from './router'
import store from './store'

Vue.use(Vuetify)
Vue.use(VueHighcharts)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
