import Vue from 'vue'
import Vuex from 'vuex'

import emrbackupfile from './modules/emrbackupfile'
import emrdata from './modules/emrdata'
import emrtype from './modules/emrtype'
import facility from './modules/facility'
import summary from './modules/summary'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    emrbackupfile,
    emrdata,
    emrtype,
    facility,
    summary
  }
})
