import axios from 'axios'
import swal from 'sweetalert'

const API_ENDPOINT = process.env.API_PUBLIC_ENDPOINT

const state = {
  emrBackupFile: {},
  emrBackupFiles: [],
  lastID: '',
  perPage: 10,
  newEmrBackupFileDialogVisible: false,
  dialogInProgress: false,
  uploadProgress: 0
}

const mutations = {
  addBackupFile (state, payload) {
    state.emrBackupFile.backupFile = payload.data
  },
  addNewEmrBackupFile (state, payload) {
    state.emrBackupFiles.push(payload.data)
  },
  showNewEmrBackupFileDialog (state, payload) {
    state.newEmrBackupFileDialogVisible = payload.data
  },
  setDialogInProgress (state, payload) {
    state.dialogInProgress = payload.data
  },
  setUploadProgress (state, payload) {
    state.uploadProgress = payload.data
  },
  setEmrBackupFile (state, payload) {
    state.emrBackupFile = payload.data
  },
  setEmrBackupFiles (state, payload) {
    state.emrBackupFiles = payload.data
  }
}

const actions = {
  addBackupFile ({ commit, state }, payload) {
    commit({ type: 'addBackupFile', data: payload })
  },
  showNewEmrBackupFileDialog ({ commit, state }, payload) {
    commit({ type: 'showNewEmrBackupFileDialog', data: payload })
  },
  refreshEmrBackupFiles ({ commit, state }) {
    commit({ type: 'setEmrBackupFiles', data: [] })
  },
  getEmrBackupFiles ({ commit, state }) {
    axios.get(API_ENDPOINT + '/emr-backup-files?&api_key=d0cedebda217433a80b3ea4f08e0271b')
    .then(function (response) {
      commit({ type: 'setEmrBackupFiles', data: response.data.emr_backup_files })
    })
    .catch(function (error) {
      console.log(error)
    })
  },
  getEmrBackupFileById ({ commit, state }, payload) {
    axios.get(API_ENDPOINT + '/emr-backup-files/' + payload.id)
    .then(function (response) {
      commit({ type: 'setEmrBackupFile', data: response.data })
    })
    .catch(function (error) {
      swal('An Error Occurred', error.message, 'error')
    })
  },
  saveEmrBackupFile ({ commit, state }) {
    commit({ type: 'setDialogInProgress', data: true })
    if (!state.emrBackupFile.facilityId || !state.emrBackupFile.emrTypeId || !state.emrBackupFile.backupFile) {
      commit({ type: 'setDialogInProgress', data: false })
      swal('Validation Error', 'Please enter all fields', 'error')
    } else {
      var formData = new FormData()
      formData.append('emr_type_id', state.emrBackupFile.emrTypeId)
      formData.append('facility_id', state.emrBackupFile.facilityId)
      formData.append('file', state.emrBackupFile.backupFile)
      axios.post(API_ENDPOINT + '/emr-backup-files?&api_key=d0cedebda217433a80b3ea4f08e0271b', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        onUploadProgress: function (e) {
          commit({ type: 'setUploadProgress', data: (e.loaded * 100) / e.total })
        }
      }).then(function (response) {
        if (response.data.status === 'SUCCESS') {
          state.emrBackupFile = {}
          commit({ type: 'showNewEmrBackupFileDialog', data: false })
          commit({ type: 'setDialogInProgress', data: false })
          swal('Success', 'EMR Backup File uploaded successfully', 'success')
          axios.get(API_ENDPOINT + '/emr-backup-files?&api_key=d0cedebda217433a80b3ea4f08e0271b')
          .then(function (response) {
            commit({ type: 'setEmrBackupFiles', data: response.data.emr_backup_files })
          })
          .catch(function (error) {
            console.log(error)
          })
        } else {
          var message = ''
          message = message + response.data.message
          if (response.data.errors) {
            for (var i = response.data.errors.length - 1; i >= 0; i--) {
              message = message + ',' + response.data.errors[i]
            }
          }
          commit({ type: 'setDialogInProgress', data: false })
          swal('Server Error', message, 'error')
        }
      })
      .catch(function (error) {
        commit({ type: 'setDialogInProgress', data: false })
        swal('An Error Occurred', error.message, 'error')
      })
    }
  },
  exportEmrBackupFiles ({ commit, state }, payload) {
    var format = 'csv'
    console.log(payload)
    var url = ''
    url = url + '&export=' + format
    window.location.href = API_ENDPOINT + '/emr-backup-files?api_key=d0cedebda217433a80b3ea4f08e0271b&' + url
  }
}

export default {
  state,
  mutations,
  actions
}
