import axios from 'axios'

const API_ENDPOINT = process.env.API_PUBLIC_ENDPOINT

const state = {
  emrTypes: []
}

const mutations = {
  setEmrTypes (state, payload) {
    state.emrTypes = payload.data
  }
}

const actions = {
  getEmrTypes ({ commit, state }) {
    axios.get(API_ENDPOINT + '/emr-types?&api_key=d0cedebda217433a80b3ea4f08e0271b')
    .then(function (response) {
      commit({ type: 'setEmrTypes', data: response.data.emr_types })
    })
    .catch(function (error) {
      state.message = { type: 'error', title: 'Emr Types not loaded', message: error.message }
    })
  }
}

export default {
  state,
  mutations,
  actions
}
