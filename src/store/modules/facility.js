import axios from 'axios'

const API_ENDPOINT = process.env.API_PUBLIC_ENDPOINT

const state = {
  importFile: {},
  facility: {},
  facilities: [],
  lastID: '',
  perPage: 10,
  showNew: false,
  showImport: false,
  message: {}
}

const mutations = {
  addImportFile (state, payload) {
    state.importFile = payload.data
  },
  addNewFacility (state, payload) {
    state.facilities.push(payload.data)
  },
  showNew (state, payload) {
    state.showNew = payload.data
  },
  showImport (state, payload) {
    state.showImport = payload.data
  },
  setFacility (state, payload) {
    state.facility = payload.data
  },
  setFacilities (state, payload) {
    state.facilities = payload.data
  }
}

const actions = {
  addImportFile ({ commit, state }, payload) {
    commit({ type: 'addImportFile', data: payload })
  },
  showNewDialog ({ commit, state }, payload) {
    commit({ type: 'showNew', data: payload })
  },
  showImportDialog ({ commit, state }, payload) {
    commit({ type: 'showImport', data: payload })
  },
  refreshFacilities ({ commit, state }) {
    commit({ type: 'setFacilities', data: [] })
  },
  getFacilities ({ commit, state }) {
    axios.get(API_ENDPOINT + '/facilities?&api_key=d0cedebda217433a80b3ea4f08e0271b')
    .then(function (response) {
      commit({ type: 'setFacilities', data: response.data.facilities })
    })
    .catch(function (error) {
      state.message = { type: 'error', title: 'Facilities not loaded', message: error.message }
    })
  },
  getFacilityById ({ commit, state }, payload) {
    axios.get(API_ENDPOINT + '/facilities/' + payload.id + '?&api_key=d0cedebda217433a80b3ea4f08e0271b')
    .then(function (response) {
      commit({ type: 'setFacility', data: response.data })
    })
    .catch(function (error) {
      state.message = { type: 'error', title: 'Facility not loaded', message: error.message }
    })
  },
  saveFacility ({ commit, state }) {
    axios.post(API_ENDPOINT + '/facilities?&api_key=d0cedebda217433a80b3ea4f08e0271b', state.facility)
    .then(function (response) {
      commit({ type: 'setFacility', data: {} })
      commit({ type: 'addNewFacility', data: response.data })
      commit({ type: 'showNew', data: false })
    })
    .catch(function (error) {
      state.message = { type: 'error', title: 'Facilitiy not saved', message: error.message }
    })
  },
  importFacility ({ commit, state }) {
    var formData = new FormData()
    formData.append('file', state.importFile)
    axios.post(API_ENDPOINT + '/facilities/import?&api_key=d0cedebda217433a80b3ea4f08e0271b', formData)
    .then(function (response) {
      state.importFile = {}
      commit({ type: 'showImport', data: false })
    })
    .catch(function (error) {
      state.message = { type: 'error', title: 'Facilities not imported', message: error.message }
    })
  },
  exportData ({ commit, state }, format) {
    var url = 'export=' + format
    window.location.href = API_ENDPOINT + '/facilities?&api_key=d0cedebda217433a80b3ea4f08e0271b&' + url
  }
}

export default {
  state,
  mutations,
  actions
}
