import axios from 'axios'

const API_ENDPOINT = process.env.API_PUBLIC_ENDPOINT

const state = {
  summaries: [],
  displayingSummaries: false,
  fetchingSummaries: false
}

const mutations = {
  setSummaries (state, payload) {
    state.summaries = payload.data
  },
  setDisplayingSummaries (state, payload) {
    state.displayingSummaries = payload.data
  },
  setFetchingSummaries (state, payload) {
    state.fetchingSummaries = payload.data
  }
}

const actions = {
  setSummaries ({ commit, state }, payload) {
    commit({ type: 'setSummaries', data: payload })
  },
  getSummaries ({ commit, state }) {
    commit({ type: 'setFetchingSummaries', data: true })
    axios.get(API_ENDPOINT + '/emr-data/summaries?&api_key=d0cedebda217433a80b3ea4f08e0271b')
    .then(function (response) {
      commit({ type: 'setDisplayingSummaries', data: true })
      commit({ type: 'setSummaries', data: response.data.summaries })
      commit({ type: 'setFetchingSummaries', data: false })
    })
    .catch(function (error) {
      console.log(error)
    })
  },
  setDisplayingSummaries ({ commit, state }, payload) {
    commit({ type: 'setDisplayingSummaries', data: payload })
  },
  setFetchingSummaries ({ commit, state }, payload) {
    commit({ type: 'setFetchingSummaries', data: payload })
  }
}

export default {
  state,
  mutations,
  actions
}
