import axios from 'axios'
import swal from 'sweetalert'

const API_ENDPOINT = process.env.API_PUBLIC_ENDPOINT

const state = {
  displayEmrData: false,
  emrData: [],
  fields: []
}

const mutations = {
  setEmrData (state, payload) {
    state.emrData = payload.data
  },
  setDisplayEmrData (state, payload) {
    state.displayEmrData = payload.data
  },
  setFields (state, payload) {
    state.fields = payload.data
  }
}

const actions = {
  getFields ({ commit, state }) {
    axios.get(API_ENDPOINT + '/emr-data/fields?api_key=d0cedebda217433a80b3ea4f08e0271b').then(function (response) {
      commit({ type: 'setFields', data: response.data.fields })
    }).catch(function (error) {
      console.log(error)
    })
  },
  getEmrData ({ commit, state }, data) {
    var queryFacilities = data.queryFacilities
    var queryFields = data.queryFields
    var conditions = data.conditions
    var emrDataType = data.emrDataType
    var format = data.format
    var url = ''
    conditions.forEach(function (cond) {
      if (cond.field && cond.condition && cond.value) {
        url = url + '&' + cond.field + '=[' + cond.condition + ',' + cond.value + ']'
      }
    })
    if (queryFacilities.length === 1) {
      url = url + '&patient.facility_id=' + queryFacilities.join('')
    } else {
      url = url + '&patient.facility_id=[in,(' + queryFacilities.join(';') + ')]'
    }
    url = url + '&fields=' + queryFields.join()
    url = url + '&type=' + emrDataType
    if (format === 'json') {
      axios.get(API_ENDPOINT + '/emr-data?api_key=d0cedebda217433a80b3ea4f08e0271b&' + url).then(function (response) {
        commit({ type: 'setEmrData', data: response.data.emr_data })
        commit({ type: 'setDisplayEmrData', data: true })
      }).catch(function (error) {
        swal('An Error Occurred', error.message, 'error')
      })
    } else {
      url = url + '&export=' + format
      window.location.href = API_ENDPOINT + '/emr-data?api_key=d0cedebda217433a80b3ea4f08e0271b&' + url
    }
  },
  setDisplayEmrData ({ commit, state }, data) {
    commit({ type: 'setDisplayEmrData', data: data })
  }
}

export default {
  state,
  mutations,
  actions
}
