var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_PRIVATE_ENDPOINT: '"http://192.168.43.59"',
  API_PUBLIC_ENDPOINT: '"http://192.168.43.59"',
  API_ENDPOINT: '"http://192.168.43.59"'
})
